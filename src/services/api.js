import { http } from "../config/axios"
import { urlParams } from "../helpers/urlParams";

const httpReqestHandler = (errors) => {

    if(errors.response && errors.response.status === 422) {
        return {
            status : errors.response.status,
            message: errors.response.data.message,
            errors: errors.response.data.data
        }
    }

    if(errors.response && errors.response.status === 401) {
        localStorage.removeItem("token");
        return;
    }

    return errors;
}

const httpResponseHandler = (response) => {
    return {
        status : 200,
        message: response.data.message,
        data: response.data.data
    };
}

export const getReqeust = async (path, params) => {
    const url = `${path}?${urlParams(params)}`;
    return await http.get(url)
        .then(response => httpResponseHandler(response))
        .catch(error => httpReqestHandler(error));
}

export const postRequest = async (path, body) => {
    return await http.post(path, body)
        .then(response => httpResponseHandler(response))
        .catch(error => httpReqestHandler(error));
}

export const putRequest = async (path, body) => {
    return await http.put(path, body)
    .then(response => httpResponseHandler(response))
    .catch(error => httpReqestHandler(error));
}