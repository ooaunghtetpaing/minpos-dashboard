import axios from "axios";

axios.defaults.baseURL = "http://localhost:8000/api";

axios.interceptors.request.use(
    async (config) => {
        const token = localStorage.getItem("token") ? localStorage.getItem("token") : null;

        if(token) {
            config.headers = {
                ...config.headers,
                authorization: `Bearer ${token}`,
                Accept: "application/json"
            };
        }

        return config;
    },
    (error) => Promise.reject(error)
);

export const http = axios;