import { createSlice } from '@reduxjs/toolkit'

export const productSlice = createSlice({
    name: 'product',
    initialState: {
        selectedProducts: []
    },
    reducers: {
        addToCart: (state, action) => {
            state.selectedProducts.push(action.payload);
            return state;
        },

        updateCart: (state, action) => {
            state.selectedProducts = action.payload;
            return state;
        }
    }
});

export const { addToCart, updateCart } = productSlice.actions;
export default productSlice.reducer;
