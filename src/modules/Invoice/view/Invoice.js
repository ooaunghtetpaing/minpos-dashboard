import numeral from "numeral";
import { useEffect, useState } from "react"
import { Trash } from "react-bootstrap-icons";
import Button from "react-bootstrap/esm/Button";
import Form from 'react-bootstrap/Form';
import { useDispatch, useSelector } from 'react-redux';
import { updateCart } from "../../../redux/productSlice";

export const Invoice = () => {
    const dispatch = useDispatch();
    const productReducer = useSelector((state) => state.product);
    const [totalAmount, setTotalAmount] = useState(0);
    const [grandAmount, setGrandAmount] = useState(0);
    const [discont, setDiscount] = useState(0);
    const [cashAmount, setCashAmount] = useState(0);
    const [refund, setRefund] = useState(0);

    const removeItem = (e) => {
        if(productReducer.selectedProducts) {
            const updateProducts = productReducer.selectedProducts.filter(value => value.id !== e.id);
            dispatch(updateCart(updateProducts));
        }
    }

    const calculateDiscount = (e) => {
        setDiscount(e);
        setGrandAmount(totalAmount - e);
    }

    const calculateCashAmount = (e) => {
        setCashAmount(Number(e));
        
        if(cashAmount > grandAmount && grandAmount) {
            console.log("sdsdssds");
            const getRefund = cashAmount - grandAmount;
            setRefund(getRefund);
        }
    }

    const openInvoice = () => {
        console.log("open Invoice");
    }

    useEffect(() => {
        if(productReducer.selectedProducts && productReducer.selectedProducts.length > 0) {
            const getTotalAmount = productReducer.selectedProducts.reduce((accumulator, object) => {
                return accumulator + (object.price * object.qty);
            }, 0);

            setTotalAmount(getTotalAmount);
            
            if(discont === 0) {
                setGrandAmount(getTotalAmount);
            }
        }
    },[]);

    return(
        <div className="row">
            <div className="col-12 mt-3">
                <div className="table-responsive mt-3">
                    <table className="table table-responsive">
                        <thead>
                            <tr>
                                <th style={{width: "200px"}}> Item </th>
                                <th style={{width: "200px"}}> Price </th>
                                <th style={{width: "200px"}}> Qty </th>
                                <th style={{width: "200px"}}> Total</th>
                                <th style={{width: "200px"}}> Action </th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            {productReducer.selectedProducts && productReducer.selectedProducts.map((value, index) => {
                                return(
                                    <tr 
                                        key={index}
                                        style={{width: "200px"}}
                                    >
                                        <td> <small> { value.code } ( { value.size }) </small> </td>
                                        <td> <small> { numeral(value.price ).format('0,0') }  MMK </small> </td>
                                        <td> <small> { value.qty } </small> </td>
                                        <td> <small> { numeral(value.total).format('0,0') } MMK </small> </td>
                                        <td> 
                                            <Trash  
                                                className="text-danger"
                                                size={20} 
                                                color={"red"}
                                                onClick={() => removeItem(value)}
                                            />
                                        </td>
                                    </tr>
                                )
                            })}

                            {productReducer.selectedProducts && productReducer.selectedProducts.length > 0 && (
                                <>
                                    <tr>
                                        <td> <small> <b> Amount </b></small>  </td>
                                        <td>  {numeral(totalAmount).format('0,0')} MMK </td>
                                    </tr>
 
                                    <tr>
                                        <td> <small> <b> Discount </b></small>  </td>
                                        <td>
                                            <Form.Group>
                                                <Form.Control 
                                                    size="sm"
                                                    type="number"
                                                    placeholder="0"
                                                    onChange={(e) => calculateDiscount(e.target.value)}
                                                />
                                            </Form.Group>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td> <small> <b> Total Amount </b></small> </td>
                                        <td> {numeral(grandAmount).format('0,0')} MMK  </td>
                                    </tr>

                                    <tr>
                                        <td> <small> <b> Cash </b></small> </td>
                                        <td> 
                                            <Form.Group>
                                                <Form.Control 
                                                    size="sm"
                                                    type="number"
                                                    placeholder="0"
                                                    onChange={(e) => calculateCashAmount(e.target.value)}
                                                />
                                            </Form.Group> 
                                        </td>
                                    </tr>
                                
                                    <tr>
                                        <td> <small> <b> Refund </b></small>  </td>
                                        <td> {numeral(refund).format('0,0')} MMK </td>
                                    </tr>
                                    <tr>
                                        <td> 
                                            <Button
                                                onClick={() => openInvoice()}
                                            > Save </Button>
                                        </td>
                                    </tr>
                                </>
                            )}
                            
                        </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    )
}