import numeral from "numeral";
import { useEffect, useState } from "react";
import DataTable from "react-data-table-component"
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import { getReqeust } from "../../../services/api";
import { Invoice } from "../../Invoice/view/Invoice";
import { SearchItem } from "../../product/entry/SearchItem";
import { useDispatch } from 'react-redux';
import { addToCart } from "../../../redux/productSlice";
export const Sale = () => {

    const dispatch = useDispatch();
    
    const [loading, setLoading] = useState(false);
    const [products, setProduct] = useState([]);
    const [selectedProdcut, setSelectedProduct] = useState(null);
    const [openModel, setOpenModel] = useState(false);
    const [total, setTotal] = useState(0);
    const [carts, setCarts] = useState([]);
    const [urlParams, setUrlParams] = useState({
        page: 1,
        per_page: 15,
        columns: "id,name,size,price,code",
        order: "id",
        sort: "DESC",
        search: ""
    });
    const columns = [
        {
            name: 'Name',
            selector: row => row.name,
            sortable: true
        },
        {
            name: 'Code',
            selector: row => row.code,
            sortable: true
        },
        {
            name: 'Size',
            selector: row => row.size,
            sortable: true
        },
        {
            name: 'Price',
            selector: row => `${numeral(row.price).format("0,0")} MMK`,
            sortable: true
        }
    ];

    const fetchProduct = async () => {
        setLoading(true);
        const response = await getReqeust('item',urlParams);

        if(response && response.status === 200) {
            setProduct(response.data.data);
            setTotal(response.data.total);
            setLoading(false);
            return;
        }

        setLoading(false);
        return;
    }

    const searchProduct = (e) => {
        let updateUrlParams = {...urlParams};
        updateUrlParams.search = e;
        setUrlParams(updateUrlParams);
        fetchProduct(updateUrlParams);
    }

    const rowsPerPageHandler = async (e) => {
        let updateUrlParams = {...urlParams};
        updateUrlParams.per_page = e;
        setUrlParams(updateUrlParams);
        await fetchProduct(updateUrlParams);
    }

    const changePageHandler = async (e) => {
        let updateUrlParams = {...urlParams};
        updateUrlParams.page = e;
        setUrlParams(updateUrlParams);
        await fetchProduct(updateUrlParams);
    }

    const addToCartList = (e) => {
        let updateSelectedProuct = {...selectedProdcut};
        updateSelectedProuct.qty = e;
        updateSelectedProuct.total = e * updateSelectedProuct.price;

        dispatch(addToCart(updateSelectedProuct));
    }

    useEffect(() => {
        setLoading(true);
        fetchProduct(urlParams);
        setLoading(false);
    },[]);

    return(
        <div className="row">
            <div className="col-12 col-md-6 col-lg-6">
                <DataTable 
                    title="Product List"
                    columns={columns}
                    data={products}
                    responsive={true}
                    persistTableHead
                    subHeader
                    subHeaderComponent={
                        <div className="sale-search-item">
                            <SearchItem 
                                search={(e) => searchProduct(e)}
                            />
                        </div>
                    }
                    pagination
			        paginationServer
                    paginationTotalRows={total}
                    paginationRowsPerPageOptions={[15,20,50,100]}
                    onRowClicked={e => {
                        setOpenModel(true);
                        setSelectedProduct(e);
                    }}
                    highlightOnHover
                    pointerOnHover
                    onChangeRowsPerPage={e => rowsPerPageHandler(e)}
                    onChangePage={e => changePageHandler(e)}
                />
            </div>

            <div className="col-12 col-md-6 col-lg-6">
                <Invoice carts={carts} />
            </div>

            <Modal show={openModel} onHide={() => setOpenModel(false)}>
                <Modal.Header closeButton>
                    <Modal.Title> Add Qty </Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form.Group>
                        <Form.Control 
                            type="number"
                            placeholder="Qty"
                            onKeyDown={(e) => {
                                if(e.key === "Enter") {
                                    setOpenModel(false);
                                    addToCartList(e.target.value);
                                }
                            }}
                            autoFocus
                        />
                    </Form.Group>
                </Modal.Body>
            </Modal>
        </div>
    )
}