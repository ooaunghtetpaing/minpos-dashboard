import { Outlet } from "react-router-dom"

export const Dashboard = () => {
    return (
        <div className="row">
            <h3> Dashboard </h3>
            <Outlet />
        </div>
    )
}