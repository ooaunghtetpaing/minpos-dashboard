import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Spinner from "react-bootstrap/esm/Spinner";
import { postRequest } from "../../../services/api";
import { ErrorMessage } from "../../../components/ErrorMessage";

export const Login = () => {
    const navigate = useNavigate();
    const token = localStorage.getItem("token") ? localStorage.getItem("token") : null;

    const [form, setForm] = useState({
        username: "",
        password: ""
    });

    const [loading, setLoading] = useState(false);
    const [errors, setErrors] = useState([]);

    const formStateHandler = (event, inputName) => {
        let updateForm = {...form};
        updateForm[inputName] = event.target.value;
        setForm(updateForm);
        return;
    }

    const login = async () => {
        setLoading(true);

        const response = await postRequest("auth/login", form);
        
        if(response.status === 422) {
            setErrors(response.errors);
            setLoading(false);
        }

        if(response.status === 200) {
            setLoading(false);
            localStorage.setItem("token", response.data.token);
            navigate("/");
        }

        return;
    }


    useEffect(() => {
        if(token) {
            navigate("dashbard");
        }
    },[]);

    return(
        <div className="container-fluid">
            <div className="row d-flex flex-column justify-content-center align-items-center">
                <div className="col-12 col-md-4 col-lg-4 mt-3">
                    <Card>
                        <Card.Header>
                            <Card.Title> Dashboard Login </Card.Title>
                        </Card.Header>
                    
                        <Card.Body>
                            <Form>
                                <Form.Group className="mb-3" controlId="Username">
                                    <Form.Label>Username</Form.Label>
                                    <Form.Control 
                                        type="text" 
                                        placeholder="Enter username or account name"
                                        required
                                        onChange={e => formStateHandler(e, 'username')}
                                    />
                                    {errors && errors['username'] && (
                                        <ErrorMessage message={errors['username'][0]} />
                                    )}
                                </Form.Group>

                                <Form.Group className="mb-3" controlId="Password">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control 
                                        type="password" 
                                        placeholder="Password" 
                                        required
                                        onChange={e => formStateHandler(e, 'password')}
                                    />
                                    {errors && errors['password'] && (
                                        <ErrorMessage message={errors['password'][0]} />
                                    )}
                                </Form.Group>

                                <Form.Group className="mb-3">
                                    <Button
                                        disabled={loading}
                                        onClick={login}
                                    >   
                                        { loading && (<Spinner animation="border" size="sm" />)}
                                        <span> Login </span>
                                    </Button>
                                </Form.Group>
                            </Form>
                        </Card.Body>
                    </Card>
                </div>
            </div>
        </div>
    )
}