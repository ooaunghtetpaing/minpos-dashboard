import Form from 'react-bootstrap/Form';
import Image from 'react-bootstrap/Image';
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/esm/Spinner';
import defaultImage from "../../../assets/images/defaultImage.png";
import { useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { postRequest } from '../../../services/api';
import { ErrorMessage } from '../../../components/ErrorMessage';

export const NewProduct = ({ refresh }) => {

    const navigate = useNavigate();
    
    const [loading, setLoading] = useState(false);
    const [errors, setErrors] = useState(null);
    const [form, setForm] = useState({
        name: "",
        code: "",
        size: "",
        price: ""
    });

    const imageFileUpload = useRef(null);
    const updateFormData = (e, fieldName) => {
        let updateForm = {...form};
        updateForm[fieldName] = e.target.value;
        setForm(updateForm);
        return;
    }

    const createProduct = async () => {
        setLoading(true);
        const response = await postRequest('item', form);

        if(response && response.status === 401) {
            setLoading(false);
            navigate('/logout');
            return;
        }

        if(response && response.status === 200) {
            refresh();
        }

        if(response && response.status === 422) {
            setErrors(response.errors);
        }

        setLoading(false);
    }

    return(
        <>
            <Form.Group className='mb-3'>
                <Image 
                    src={defaultImage}
                    thumbnail
                />
            </Form.Group>

            <Form.Group className='mb-3'>
                <Form.Control
                    type='text'
                    placeholder='Name'
                    defaultValue={form.name}
                    onChange={(e) => updateFormData(e, "name")}
                />
                {errors && errors['name'] && (<ErrorMessage message={errors['name']} />)}
            </Form.Group>

            <Form.Group className='mb-3'>
                <Form.Control
                    type='text'
                    placeholder='Code'
                    onChange={(e) => updateFormData(e, "code")}
                />
                {errors && errors['code'] && (<ErrorMessage message={errors['code']} />)}
            </Form.Group>

            <Form.Group className='mb-3'>
                <Form.Control
                    type='text'
                    placeholder='Size'
                    onChange={(e) => updateFormData(e, "size")}
                />
                {errors && errors['size'] && (<ErrorMessage message={errors['size']} />)}
            </Form.Group>

            <Form.Group className='mb-3'>
                <Form.Control
                    type='text'
                    placeholder='Price'
                    onChange={(e) => updateFormData(e, "price")}
                />
                {errors && errors['price'] && (<ErrorMessage message={errors['price']} />)}
            </Form.Group>

            <Form.Group className='mb-3'>
                <Button
                    disabled={loading}
                    onClick={() => createProduct()}
                >
                    { loading && (<Spinner animation='border' />)}
                    <span> Create </span>
                </Button>
            </Form.Group>
        </>
    )
}