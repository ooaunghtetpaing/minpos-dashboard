import Image from 'react-bootstrap/Image';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import defaultImage from "../../../assets/images/defaultImage.png";
import Spinner from 'react-bootstrap/esm/Spinner';
import { useEffect, useRef, useState } from 'react';
import { putRequest } from '../../../services/api';
import { imageFileRender } from '../../../helpers/fileRender';

export const ProductDetail = ({data, fetchData}) => {

    const [loading, setLoading] = useState(false);
    const [form, setForm] = useState({});
    const imageFileUpload = useRef(null);

    const updateProduct = async () => {
        setLoading(true);

        const response = await putRequest(`item/${data.id}`, form);

        if(response.status === 200) {
            fetchData();
        }
        
        setLoading(false);
    }

    const updateFormData = (e, fieldName) => {
        let updateForm = {...form};
        updateForm[fieldName] = e.target.value;
        setForm(updateForm);
        return;
    }

    const handleChange = (event) => {
        const fileUploaded = event.target.files[0];
        imageFileRender(event.target.files[0]);
    }

    const handleClick = () => {
        imageFileUpload.current.click();
    }

    useEffect(() => {
        setForm(data);
    },[]);

    return(
        <div className="row my-3">
            <div className="col-12 col-md-3 col-lg-3">
                <Image 
                    className='product-item'
                    src={defaultImage} 
                    thumbnail
                    onClick={handleClick}
                />

                <input 
                    type="file"
                    ref={imageFileUpload} 
                    onChange={handleChange}
                    style={{display: 'none'}}
                />
            </div>

            <div className='col-12 col-md-9 col-lg-9'>
                <Form.Group className='mb-3'>
                    <Form.Control 
                        type='text'
                        placeholder='Enter product name'
                        defaultValue={form.name}
                        onChange={e => updateFormData(e, 'name')}
                    />
                </Form.Group>

                <Form.Group className='mb-3'>
                    <Form.Control 
                        type='text'
                        placeholder='Enter product code number'
                        defaultValue={form.code}
                        onChange={e => updateFormData(e, 'code')}
                    />
                </Form.Group>

                <Form.Group className='mb-3'>
                    <Form.Control 
                        type='text'
                        placeholder='Enter size'
                        defaultValue={form.size}
                        onChange={e => updateFormData(e, 'size')}
                    />
                </Form.Group>

                <Form.Group className='mb-3'>
                    <Form.Control 
                        type='text'
                        placeholder='Enter price'
                        defaultValue={form.price}
                        onChange={e => updateFormData(e, 'price')}
                    />
                </Form.Group>

                <Form.Group className='mb-3'>
                    <Button 
                        disabled={loading}
                        onClick={updateProduct}
                    > 
                        {loading && (<Spinner animation='border' size='sm'/>)}
                        <span> Update </span>
                    </Button>
                </Form.Group>
            </div>
        </div>
    )
}