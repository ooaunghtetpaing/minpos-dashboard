import { useEffect, useState } from "react"
import DataTable from "react-data-table-component";
import { getReqeust } from "../../../services/api";
import { ProductDetail } from "./ProductDetail";
import numeral from "numeral";
import { SearchItem } from "../entry/SearchItem";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { NewProduct } from "../entry/NewProduct";

export const Product = () => {

    const [loading, setLoading] = useState(false);
    const [data, setData] = useState([]);
    const [total, setTotal] = useState(0);
    const [openModel, setOpenModel] = useState(false);
    const [urlParams, setUrlParams] = useState({
        page: 1,
        per_page: 15,
        columns: "id,name,size,price,code",
        order: "id",
        sort: "DESC",
        search: ""
    });
    const columns = [
        {
            name: "#",
            selector: (row, index) => index + 1
        },
        {
            name: 'Name',
            selector: row => row.name,
            sortable: true
        },
        {
            name: 'Code',
            selector: row => row.code,
            sortable: true
        },
        {
            name: 'Size',
            selector: row => row.size,
            sortable: true
        },
        {
            name: 'Price',
            selector: row => `${numeral(row.price).format("0,0")} MMK`,
            sortable: true
        }
    ];

    const fetchData = async (urlParams) => {
        setLoading(true);
        const response = await getReqeust('item',urlParams);

        if(response && response.status === 200) {
            setData(response.data.data);
            setTotal(response.data.total);
            setLoading(false);
            return;
        }

        setLoading(false);
        return;
    }

    const rowsPerPageHandler = async (e) => {
        let updateUrlParams = {...urlParams};
        updateUrlParams.per_page = e;
        setUrlParams(updateUrlParams);
        await fetchData(updateUrlParams);
    }

    const changePageHandler = async (e) => {
        let updateUrlParams = {...urlParams};
        updateUrlParams.page = e;
        setUrlParams(updateUrlParams);
        await fetchData(updateUrlParams);
    }

    const searchProduct = (e) => {
        let updateUrlParams = {...urlParams};
        updateUrlParams.search = e;
        setUrlParams(updateUrlParams);
        fetchData(updateUrlParams);
    }

    useEffect(() => {
        setLoading(true);
        fetchData(urlParams);
        setLoading(false);
    },[]);

    return(
        <div className="row">
            <div className="col-12">
                <DataTable 
                    title="Product List"
                    responsive={true}
                    persistTableHead
                    subHeader
                    subHeaderComponent={
                        <>
                            <SearchItem search={(e) => searchProduct(e)}/>
                            <Button 
                                className='border-radius-right' 
                                onClick={() => setOpenModel(!openModel)}
                            > 
                                Create 
                            </Button>
                        </>
                    }
                    data={data}
                    columns={columns}
                    pagination
			        paginationServer
                    expandableRows
                    expandableRowsComponent={ProductDetail}
                    expandableRowsComponentProps={{ "fetchData" : () => fetchData(urlParams) }}
                    paginationTotalRows={total}
                    paginationRowsPerPageOptions={[15,20,50,100]}
                    onChangeRowsPerPage={e => rowsPerPageHandler(e)}
                    onChangePage={e => changePageHandler(e)}
                />
            </div>

            <Modal show={openModel} onHide={() => setOpenModel(false)}>
                <Modal.Header closeButton>
                    <Modal.Title> Create New Product </Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <NewProduct refresh={() => {
                        setOpenModel(false);
                        fetchData(urlParams);
                    }} />
                </Modal.Body>
            </Modal>
        </div>
    )
}