import { useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom"
import { Toolbar } from "../components/Toolbar";

export const Layout = () => {
    const navigate = useNavigate();
    const token = localStorage.getItem("token") ? localStorage.getItem("token") : null;

    useEffect(() => {
        if(token === null) {
            navigate("auth/login");
        }
    },[]);

    return(
        <div className="container-fluid">
            <Toolbar />
            <Outlet />
        </div>
    )
}