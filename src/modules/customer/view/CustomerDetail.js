import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Spinner from 'react-bootstrap/esm/Spinner';
import { useEffect, useState } from 'react';
import { putRequest } from '../../../services/api';
import { useNavigate } from 'react-router-dom';
import { ErrorMessage } from '../../../components/ErrorMessage';

export const CustomerDetail = ({data, fetchData}) => {

    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    const [errors, setErrors] = useState(null);
    const [payload, setPayload] = useState({
        name: "",
        email: "",
        phone: "",
        address: ""
    });

    const updateCustomer = async () => {
        setLoading(true);

        const response = await putRequest(`customer/${data.id}`, payload);

        if(response && response.status === 422) {
            setErrors(response.errors);
        }

        if(response && response.status === 200) {
            fetchData();
        }

        if(response && response.status === 401) {
            setLoading(false);
            navigate("/logout");
            return;
        }
        
        setLoading(false);
    }

    const updatePayload = (e, fieldName) => {
        let updatePayload = {...payload};
        updatePayload[fieldName] = e.target.value ? e.target.value : null;
        setPayload(updatePayload);
        return;
    }

    useEffect(() => {
        setPayload(data);
    },[data]);

    return(
        <div className="row my-3">
            <div className='col-12 col-md-9 col-lg-9'>
                <Form.Group className='mb-3'>
                    <Form.Control 
                        type='text'
                        placeholder='Enter customer name'
                        defaultValue={payload.name}
                        onChange={e => updatePayload(e, 'name')}
                    />
                    {errors && errors['name'] && (<ErrorMessage message={errors['name']} />)}
                </Form.Group>

                <Form.Group className='mb-3'>
                    <Form.Control 
                        type='text'
                        placeholder='Enter email address'
                        defaultValue={payload.email}
                        onChange={e => updatePayload(e, 'email')}
                    />
                    {errors && errors['email'] && (<ErrorMessage message={errors['email']} />)}
                </Form.Group>

                <Form.Group className='mb-3'>
                    <Form.Control 
                        type='text'
                        placeholder='Enter phone number'
                        defaultValue={payload.phone}
                        onChange={e => updatePayload(e, 'phone')}
                    />
                    {errors && errors['phone'] && (<ErrorMessage message={errors['phone']} />)}
                </Form.Group>

                <Form.Group className='mb-3'>
                    <Form.Control 
                        type='text'
                        placeholder='Enter Address'
                        defaultValue={payload.address}
                        onChange={e => updatePayload(e, 'address')}
                    />
                    {errors && errors['address'] && (<ErrorMessage message={errors['address']} />)}
                </Form.Group>

                <Form.Group className='mb-3'>
                    <Button 
                        disabled={loading}
                        onClick={updateCustomer}
                    > 
                        {loading && (<Spinner animation='border' size='sm'/>)}
                        <span> Update </span>
                    </Button>
                </Form.Group>
            </div>
        </div>
    )
}