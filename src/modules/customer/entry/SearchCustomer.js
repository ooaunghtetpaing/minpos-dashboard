import Form from 'react-bootstrap/Form';

export const SearchCustomer = ({search}) => {
    
    return(
        <div className="row">
            <div className="col-12">
                <Form.Group>
                    <Form.Control 
                        className='border-radius-left'
                        placeholder='Search Customer'
                        onKeyDown={(e) => {
                            if(e.key === 'Enter') {
                                search(e.target.value);
                            }
                        }}
                    />
                </Form.Group>
            </div>
        </div>
    )
}