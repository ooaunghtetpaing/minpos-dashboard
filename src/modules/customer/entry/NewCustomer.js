import Form from 'react-bootstrap/Form';
import Image from 'react-bootstrap/Image';
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/esm/Spinner';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { postRequest } from '../../../services/api';
import { ErrorMessage } from '../../../components/ErrorMessage';

export const NewCustomer = ({ refresh }) => {

    const navigate = useNavigate();
    
    const [loading, setLoading] = useState(false);
    const [errors, setErrors] = useState(null);
    const [payload, setPayload] = useState({
        name: "",
        phone: "",
        email: "",
        address: ""
    });

    const updatePayload = (e, fieldName) => {
        let updatePayload = {...payload};
        updatePayload[fieldName] = e.target.value;
        setPayload(updatePayload);
        return;
    }

    const createCustomer = async () => {
        setLoading(true);
        const response = await postRequest('customer', payload);

        if(response && response.status === 401) {
            setLoading(false);
            navigate('/logout');
            return;
        }

        if(response && response.status === 200) {
            refresh();
        }

        if(response && response.status === 422) {
            setErrors(response.errors);
        }

        setLoading(false);
    }

    return(
        <>
            <Form.Group className='mb-3'>
                <Form.Control
                    type='text'
                    placeholder='Name'
                    defaultValue={payload.name}
                    onChange={(e) => updatePayload(e, "name")}
                />
                {errors && errors['name'] && (<ErrorMessage message={errors['name']} />)}
            </Form.Group>

            <Form.Group className='mb-3'>
                <Form.Control
                    type='text'
                    placeholder='phone'
                    onChange={(e) => updatePayload(e, "phone")}
                />
                {errors && errors['phone'] && (<ErrorMessage message={errors['phone']} />)}
            </Form.Group>

            <Form.Group className='mb-3'>
                <Form.Control
                    type='text'
                    placeholder='email'
                    onChange={(e) => updatePayload(e, "email")}
                />
                {errors && errors['email'] && (<ErrorMessage message={errors['email']} />)}
            </Form.Group>

            <Form.Group className='mb-3'>
                <Form.Control
                    type='text'
                    placeholder='Address'
                    onChange={(e) => updatePayload(e, "address")}
                />
                {errors && errors['address'] && (<ErrorMessage message={errors['address']} />)}
            </Form.Group>

            <Form.Group className='mb-3'>
                <Button
                    disabled={loading}
                    onClick={() => createCustomer()}
                >
                    { loading && (<Spinner animation='border' />)}
                    <span> Create </span>
                </Button>
            </Form.Group>
        </>
    )
}