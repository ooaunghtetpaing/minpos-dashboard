import { createBrowserRouter } from "react-router-dom";
import { Loader } from "./components/Loader";
import { Login } from "./modules/auth/view/Login";
import { Customer } from "./modules/customer/view/Customer";
import { Dashboard } from "./modules/dashboard";
import { Layout } from "./modules/Layout";
import { Product } from "./modules/product/view/Product";
import { Sale } from "./modules/sale/view/Sale";
import { NotFound } from "./pages/NotFound";

export const routers = createBrowserRouter([
  {
    path: "",
    element: <Layout />,
    loader: () => <Loader />,
    errorElement: <NotFound />,
    children: [
      {
        path: "dashboard",
        element: <Dashboard />,
        loader: () => <Loader />,
        errorElement: <NotFound />,
      },
      {
        path: "product",
        element: <Product />,
        loader: () => <Loader />,
        errorElement: <NotFound />,
      },
      {
        path: "customer",
        element: <Customer />,
        loader: () => <Loader />,
        errorElement: <NotFound />,
      },
      {
        path: "sale",
        element: <Sale />,
        loader: () => <Loader />,
        errorElement: <NotFound />,
      }
    ]
  },
  {
    path: "auth",
    loader: () => <Loader />,
    errorElement: <NotFound />,
    children: [
      {
        path: "login",
        element: <Login />,
        errorElement: <NotFound />,
      }
    ]
  },
    // {
    //   path: "dashboard",
    //   element: < Dashboard />,
    //   children: [
    //     {
    //       path: "report",
    //       element: <Team />,
    //     },
    //   ],
    // },
]);