export const imageFileRender = (file) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    
    reader.onloadend = (e) => {
        console.log(e);
    }
}