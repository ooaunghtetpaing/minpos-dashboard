
export const urlParams = (params) => {
    let paramsArray = [];
    Object.keys(params).map((value) => {
        paramsArray.push(`${value}=${params[value]}`);
    });
    return paramsArray.join("&");
}