import Button from 'react-bootstrap/Button';
import { useNavigate } from 'react-router-dom';

export const NotFound = () => {

    const navigate =  useNavigate();
    return(
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="mt-3 d-flex flex-column align-items-center justify-content-center">
                        <h4> Route Not Found </h4>

                       <Button variant="primary" onClick={() => navigate('/')}> BackTo Home </Button>
                    </div>
                </div>
            </div>
        </div>
    )
}