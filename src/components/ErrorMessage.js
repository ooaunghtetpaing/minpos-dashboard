import Form from 'react-bootstrap/Form';

export const ErrorMessage = ({message}) => {
    return(
        <Form.Text className="text-danger"> {message} </Form.Text>
    )
}