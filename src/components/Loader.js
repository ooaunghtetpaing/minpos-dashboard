import Spinner from 'react-bootstrap/Spinner';

export const Loader = () => {
    return(
        <div className='d-fle flex-columns align-items-center justify-content-center'>
            <Spinner animation="border" role="status"/>
        </div>
    )
}