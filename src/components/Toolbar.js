import { useNavigate } from "react-router-dom";
import { Basket, FileBarGraph, People, WindowDesktop, FileCheckFill, Power } from 'react-bootstrap-icons';

export const Toolbar = () => {
    const menu = [
        {
            label: "Dashboard",
            route: "dashboard",
            icon: <WindowDesktop size={25}/>
        },
        {
            label: "Sale",
            route: "sale",
            icon: <Basket size={25} />
        },
        {
            label: "Invoice",
            route: "invoice",
            icon: <FileCheckFill size={25} />
        },
        {
            label: "Customer",
            route: "customer",
            icon: <People size={25} />
        },
        {
            label: "Product",
            route: "product",
            icon: <FileBarGraph size={25} />
        },
        {
            label: "Logout",
            route: "logout",
            icon: <Power size={25} />
        }
    ];

    const navigate = useNavigate();

    return(
        <div className="row">
            <div className="col-12 toolbar">
                <ul className="menu-list">
                    { menu.map((value, index) => {
                        return(
                            <li 
                                className="menu-link"
                                key={`nav_${index}`} 
                                onClick={() => navigate(value.route)}
                            > 
                                <span> {value.icon} </span>
                                <span> {value.label} </span>
                                
                            </li>
                        )
                    })}
                </ul>
            </div>
        </div>
    )
}